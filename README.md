# Audit

Analyse du site https://plaidoyer.simplon.co/

Démarche : 

- Analyse GreenIT
- Livre "Ecoconception web/Les 115 bonnes pratiques"
- Utilisation d'outils/extensions pour analyser le site
- Documentation sur le web

Le site : 

- C'est un site statique conçu en quatre rubriques/chapitres informatifs à lire
- Il ne s'agit pas d'un site dynamique, il n'est donc peut-être pas très attractif mais il s'agit d'un site informatif donc il n'y a pas besoin d'en faire des tonnes. Pour moi il est bien, et il fait ce pour quoi il a été conçu, c'est à dire informer.
- Le texte est centré au milieu en une colonne, il fait donc un peu "bloc" et rend la page très longue avec pas mal d'espace vide sur les côtés. Il serait peut-être mieux d'élargir un peu la zone de texte pour que cela soit un peu plus agréable pour les yeux.


Accessibilité : 

- Couleurs ok (vérifié avec l'extension WCAG contrast checker)
- Index des titres pas toujours clair (vérifié avec l'extension HeadingsMap)
- Alternatives textuelles sur les images pourraient être plus détaillées (vérifié avec l'extension web developer)
- Code ok vérifié avec le validateur W3C : signalement de 11 erreurs "minimes" seulement


Performance : 

Selon l'outil Website Grader, score de 88 (bien).
- Pages légères
- Peu de requêtes http
- Temps de chargement de la page assez rapide mais peut être amélioré (2,2 secondes sur les pas plus de 3s conseillées)
- Responsif ok pour version smartphone
- Sécurité ok (vérif certificat SSL)
Ce qui pose problème : 
- La compression : les images sont très grandes pour la plupart et ne sont pas redimensionnées au préalable mais avec le CSS (vérifié avec l'extension WAVE - Web Accessibility Evaluation Tools)
- Le plan du site ou sa structure peut être améliorée pour augmenter la performance également


Ecoconception :

Ecoindex A, donc ok. Bonnes pratiques toutes ok sauf : 
- Pas de print css
- 1 erreur javascript
- 1 inline stylesheet
- 9 images retaillées dans le navigateur
- 2 polices de caractères spécifiques pour au moins 1 frame


Préconisations par ordre de priorité : 

1) La priorité semble être la taille des images. Il faudrait les redimensionner au préalable et non avec le CSS car elles sont très volumineuses pour certaines et sont chargées à taille réelle avant d'être redimensionnées par le CSS.
--> Soit avec un logiciel de retouche d'images soit avec un outil de compression d'images comme le site https://tinypng.com/

2) Fournir une print css, indispensable afin de ne pas avoir à utiliser trop de feuilles à imprimer. 
--> Utiliser les media queries notamment la règle @media print et supprimer tout ce qui ne sert à rien dans l'impression, réduire la taille de police si nécessaire etc.

3) Externaliser le css présent dans le html si possible pour ne pas qu'il soit chargé à chaque page pour rien

4) Préconiser des polices standards afin d'économiser de la bande passante et accélerer le temps d'affichage des pages

5) Il faudrait peut-être revoir ensuite le plan du site. Le bouton sommaire aide à la lecture, mais selon l'extension HeadingsMap, la structure du plan du site n'est pas claire dans le code.



